<?php

require_once 'vendor/autoload.php';

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

define('__ROOT__',__DIR__);
define('__CONFIG__', __DIR__ . '/config');
define('__RESOURCES__', __DIR__ . '/resources');
define('__CACHE__', __DIR__ . '/var/cache');
define('__IMAGES__', __DIR__ . '/var/images');
define('__LOGS__', __DIR__ . '/var/logs');
