<?php

declare(strict_types=1);


namespace AlbertTrias\Performance\Test\Infrastructure\Repository;

use AlbertTrias\Performance\Domain\Entity\Image;
use AlbertTrias\Performance\Infrastructure\Repository\MysqlDatabaseRepository;
use PHPUnit\Framework\TestCase;

final class MysqlDatabaseRepositoryTest extends TestCase
{
    /** @var MysqlDatabaseRepository $repository */
    private $repository;

    protected function setUp()
    {
        parent::setUp();

        $this->repository = new MysqlDatabaseRepository('image_database_test');
    }

    /** @test */
    public function it_should_save_the_image_correctly()
    {
        $image = new Image();
        $image->setName('Name');
        $image->setUrl('http://name.com');

        $this->repository->save($image);

        $this->assertEquals($image, $this->repository->fetch($image->getId()));
    }

    /** @test */
    public function it_should_update_the_image_correctly()
    {
        $image = new Image();
        $image->setName('Name');
        $image->setUrl('http://name.com');

        $this->repository->save($image);

        $image->setName('Name2');
        $image->setTags('Taaaaags');

        $this->repository->update($image);
        $this->assertEquals($image, $this->repository->fetch($image->getId()));
    }

    /** @test */
    public function it_should_delete_the_image_correctly()
    {
        $image = new Image();
        $image->setName('Name');
        $image->setUrl('http://name.com');

        $this->repository->save($image);

        $this->assertTrue($this->repository->delete($image->getId()));
    }

    /** @test */
    public function it_should_delete_all_images()
    {
        $images = $this->repository->fetchAll();
        /** @var Image $image */
        foreach($images as $image) {
            $this->repository->delete($image->getId());
        }

        $this->assertTrue(empty($images = $this->repository->fetchAll()));
    }

    protected function tearDown()
    {
        parent::tearDown();
    }
}