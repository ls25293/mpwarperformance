<?php

declare(strict_types=1);

namespace AlbertTrias\Performance\Test\Infrastructure\Repository;

use AlbertTrias\Performance\Domain\Entity\Image;
use AlbertTrias\Performance\Domain\Repository\CacheRepository;
use AlbertTrias\Performance\Infrastructure\Repository\RedisCacheRepository;
use PHPUnit\Framework\TestCase;

final class RedisCacheRepositoryTest extends TestCase
{
    /** @var CacheRepository $repository */
    private $repository;

    protected function setUp()
    {
        parent::setUp();
        $this->repository = new RedisCacheRepository();
    }

    /** @test */
    public function it_should_save_the_image_correctly()
    {
        $image = new Image();
        $image->setId(9);
        $image->setName('Name');
        $image->setUrl('http://name.com');

        $this->repository->store($image);

        $this->assertEquals($image, $this->repository->fetch($image->getId()));
    }

    /** @test */
    public function it_should_update_the_image_correctly()
    {
        $image = new Image();
        $image->setId(10);
        $image->setName('Name');
        $image->setUrl('http://name.com');

        $this->repository->store($image);

        $image->setName('Name2');
        $image->setTags('Taaaaags');

        $this->repository->store($image);
        $this->assertEquals($image, $this->repository->fetch($image->getId()));
    }

    /** @test */
    public function it_should_delete_the_image_correctly()
    {
        $image = new Image();
        $image->setId(11);
        $image->setName('Name');
        $image->setUrl('http://name.com');

        $this->repository->store($image);

        $this->assertTrue($this->repository->delete($image->getId()));
    }

    /** @test */
    public function it_should_delete_all_images()
    {
        $images = $this->repository->fetchAll();
        /** @var Image $image */
        foreach($images as $image) {
            $this->repository->delete($image->getId());
        }

        $images = $this->repository->fetchAll();

        $this->assertTrue(empty($images));
    }

    protected function tearDown()
    {
        parent::tearDown();
    }
}