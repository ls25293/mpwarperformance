<?php

declare(strict_types=1);

namespace AlbertTrias\Performance\Test\Infrastructure\Repository;

use AlbertTrias\Performance\Domain\Entity\Image;
use AlbertTrias\Performance\Domain\Repository\SearchRepository;
use AlbertTrias\Performance\Infrastructure\Repository\ElasticSearchRepository;
use PHPUnit\Framework\TestCase;

final class ElasticSearchRepositoryTest extends TestCase
{
    /** @var SearchRepository $repository */
    private $repository;

    protected function setUp()
    {
        parent::setUp();

        $this->repository = new ElasticSearchRepository();
    }

    /** @test */
    public function it_should_save_the_image_correctly()
    {
        $image = new Image();
        $image->setId(9);
        $image->setName('name');
        $image->setUrl('http://name.com');

        $this->repository->addElementToSearch($image);

        $this->assertEquals($image, $this->repository->find($image->getId()));
    }

    /** @test */
    public function it_should_update_the_image_correctly()
    {
        $image = new Image();
        $image->setId(10);
        $image->setName('name');
        $image->setUrl('http://name.com');

        $this->repository->addElementToSearch($image);

        $image->setName('name2');
        $image->setTags('name, name2');

        $this->repository->updateElementFromSearch($image);
        $this->assertEquals($image, $this->repository->find($image->getId()));
    }

    /*
    public function it_should_search_by_field_correctly(string $field, string $value)
    {
        $image = new Image();
        $image->setId(13);
        $image->setName('name');
        $image->setUrl('http://name.com');
        $image->setTags('name');
        $image->setDescription('name');

        $this->repository->addElementToSearch($image);

        $results = $this->repository->searchBy($field, $value);

        $ids = array_map(function($searchImage) {
            return $searchImage->getId();
        }, $results);

        $this->assertContains($image->getId(), $ids);
    }
    */

    /** @test */
    public function it_should_delete_the_image_correctly()
    {
        $image = new Image();
        $image->setId(11);
        $image->setName('name');
        $image->setUrl('http://name.com');

        $this->repository->addElementToSearch($image);

        $this->assertTrue($this->repository->delete($image->getId()));
    }

    /** @test */
    public function it_should_delete_all_images()
    {
        $images = $this->repository->searchBy('name','name');
        /** @var Image $image */
        foreach($images as $image) {
            $this->repository->delete($image->getId());
        }

        $images = $this->repository->searchBy('name','Name');

        $this->assertTrue(empty($images));
    }

    public function fieldsAndValues()
    {
        return [
            "searching by name" => [
                "field" => "name",
                "value" => "name",
            ],
            "searching by tags" => [
                "field" => "tags",
                "value" => "name",
            ],
            "searching by description" => [
                "field" => "description",
                "value" => "name",
            ]
        ];
    }

    protected function tearDown()
    {
        parent::tearDown();
    }
}