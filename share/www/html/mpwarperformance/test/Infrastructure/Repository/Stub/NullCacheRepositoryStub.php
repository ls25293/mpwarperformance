<?php

declare(strict_types=1);

namespace AlbertTrias\Performance\Test\Infrastructure\Repository\Stub;

use AlbertTrias\Performance\Domain\Entity\Image;
use AlbertTrias\Performance\Domain\Repository\CacheRepository;

final class NullCacheRepositoryStub implements CacheRepository
{
    public function store(Image $image): bool
    {
        return false;
    }

    public function fetchAll(): array
    {
        return array();
    }

    public function fetch(int $id): ?Image
    {
        return null;
    }

    public function searchBy(string $query, string $value): array
    {
        return array();
    }

    public function delete(int $id): bool
    {
        return false;
    }
}