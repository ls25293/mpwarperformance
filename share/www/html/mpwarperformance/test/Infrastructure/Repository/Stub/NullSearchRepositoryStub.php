<?php

declare(strict_types=1);

namespace AlbertTrias\Performance\Test\Infrastructure\Repository\Stub;

use AlbertTrias\Performance\Domain\Entity\Image;
use AlbertTrias\Performance\Domain\Repository\SearchRepository;

final class NullSearchRepositoryStub implements SearchRepository
{

    public function searchBy(string $query, string $value): array
    {
        return array();
    }

    public function updateElementFromSearch(Image $image): bool
    {
        return false;
    }

    public function addElementToSearch(Image $image): bool
    {
        return false;
    }

    public function delete(int $id): bool
    {
        return false;
    }

    public function find(int $id): ?Image
    {
        return null;
    }
}