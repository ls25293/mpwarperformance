<?php

declare(strict_types=1);


namespace AlbertTrias\Performance\Test\Infrastructure\Repository\Stub;


use AlbertTrias\Performance\Domain\Entity\Image;
use AlbertTrias\Performance\Domain\Repository\DatabaseRepository;

final class NullDatabaseRepositoryStub implements DatabaseRepository
{

    public function save(Image $image): bool
    {
        return false;
    }

    public function fetchAll(): array
    {
        return array();
    }

    public function fetch(int $id): ?Image
    {
        return null;
    }

    public function delete(int $id): bool
    {
        return false;
    }

    public function update(Image $image): ?Image
    {
        return null;
    }

    public function searchBy(string $query, string $value): array
    {
        return array();
    }
}