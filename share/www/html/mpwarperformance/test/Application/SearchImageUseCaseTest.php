<?php

declare(strict_types=1);

namespace AlbertTrias\Performance\Test\Application;

use AlbertTrias\Performance\Application\SearchImageUseCase;
use AlbertTrias\Performance\Domain\Entity\Image;
use AlbertTrias\Performance\Domain\Repository\CacheRepository;
use AlbertTrias\Performance\Domain\Repository\DatabaseRepository;
use AlbertTrias\Performance\Domain\Repository\SearchRepository;
use AlbertTrias\Performance\Infrastructure\Repository\ElasticSearchRepository;
use AlbertTrias\Performance\Infrastructure\Repository\MysqlDatabaseRepository;
use AlbertTrias\Performance\Infrastructure\Repository\RedisCacheRepository;
use AlbertTrias\Performance\Test\Infrastructure\Repository\Stub\NullCacheRepositoryStub;
use AlbertTrias\Performance\Test\Infrastructure\Repository\Stub\NullDatabaseRepositoryStub;
use AlbertTrias\Performance\Test\Infrastructure\Repository\Stub\NullSearchRepositoryStub;
use PHPUnit\Framework\TestCase;

final class SearchImageUseCaseTest extends TestCase
{
    /** @var SearchImageUseCase $searchImageUseCaseWithDatabase */
    private $searchImageUseCaseWithDatabase;
    /** @var SearchImageUseCase $searchImageUseCaseWithCache */
    private $searchImageUseCaseWithCache;
    /** @var SearchImageUseCase $searchImageUseCaseWithSearch */
    private $searchImageUseCaseWithSearch;
    /** @var SearchImageUseCase $searchImageUseCaseWithEverything */
    private $searchImageUseCaseWithEverything;
    /** @var DatabaseRepository $dbRepository */
    private $dbRepository;
    /** @var CacheRepository $cacheRepository */
    private $cacheRepository;
    /** @var SearchRepository $searchRepository */
    private $searchRepository;

    protected function setUp()
    {
        parent::setUp();

        $this->dbRepository = new MysqlDatabaseRepository('image_database_test');
        $this->cacheRepository = new RedisCacheRepository();
        $this->searchRepository = new ElasticSearchRepository();

        $this->searchImageUseCaseWithDatabase = new SearchImageUseCase(
            $this->dbRepository,
            new NullCacheRepositoryStub(),
            new NullSearchRepositoryStub()
        );

        $this->searchImageUseCaseWithCache = new SearchImageUseCase(
            new NullDatabaseRepositoryStub(),
            $this->cacheRepository,
            new NullSearchRepositoryStub()
        );

        $this->searchImageUseCaseWithSearch = new SearchImageUseCase(
            new NullDatabaseRepositoryStub(),
            new NullCacheRepositoryStub(),
            $this->searchRepository
        );

        $this->searchImageUseCaseWithEverything = new SearchImageUseCase(
            $this->dbRepository,
            $this->cacheRepository,
            new NullSearchRepositoryStub()
        );

        $image = new Image();
        $image->setUrl('http://testing.com');
        $image->setName('testing');
        $image->setDescription('testing');
        $image->setTags('testing');

        $this->dbRepository->save($image);
        $this->cacheRepository->store($image);
        //$this->searchRepository->addElementToSearch($image);

    }

    /**
     * @test
     * @dataProvider fieldSearch
     * @param string $field
     */
    public function it_should_return_the_same_for_db_and_cache(string $field)
    {
        $resultsDb = $this->searchImageUseCaseWithDatabase->__invoke($field, "testing");
        $resultsCache = $this->searchImageUseCaseWithCache->__invoke($field, "testing");

        $compareArray = array_filter($resultsDb, function(Image $dbImage) use ($resultsCache) {
            /** @var Image $cacheImage */
            foreach($resultsCache as $cacheImage) {
                if ($dbImage->getId() === $cacheImage->getId()) return false;
            }
            return true;
        });

        $this->assertTrue(count($compareArray) === 0);
    }

    /*
    public function it_should_return_the_same_for_db_and_elastic(string $field)
    {
        $resultsDb = $this->searchImageUseCaseWithDatabase->__invoke($field, "testing");
        $resultsElastic = $this->searchImageUseCaseWithSearch->__invoke($field, "testing");

        var_dump($resultsDb);
        var_dump($resultsElastic);

        $compareArray = array_filter($resultsDb, function(Image $dbImage) use ($resultsElastic) {
            foreach($resultsElastic as $elasticImage) {
                if ($elasticImage->getId() === $dbImage->getId()) return false;
            }

            return true;
        });

        $this->assertTrue(count($compareArray) === 0);
    }
    */


    /**
     * @test
     * @dataProvider fieldSearch
     * @param string $field
     */
    public function it_should_return_an_empty_array_if_search_is_unsuccessful(string $field)
    {
        $this->assertEquals(
            array(),
            $this->searchImageUseCaseWithEverything->__invoke($field, "THIS DOES NOT EXIST")
        );
    }

    /** @test */
    public function it_should_return_an_empty_array_if_field_does_not_exist()
    {
        $this->assertEquals(
            array(),
            $this->searchImageUseCaseWithEverything->__invoke("YEAH", "THIS DOES NOT EXIST")
        );
    }


    public function fieldSearch()
    {
        return [
            "searching by name" => [
                "field" => "name",
            ],
            "searching by tags" => [
                "field" => "tags",
            ],
            "searching by description" => [
                "field" => "description",
            ]
        ];
    }

    protected function tearDown()
    {
        parent::tearDown();
    }

}