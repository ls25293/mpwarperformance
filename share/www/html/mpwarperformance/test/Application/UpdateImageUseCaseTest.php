<?php

declare(strict_types=1);

namespace AlbertTrias\Performance\Test\Application;

use AlbertTrias\Performance\Application\UpdateImageUseCase;
use AlbertTrias\Performance\Domain\Entity\Image;
use AlbertTrias\Performance\Domain\Repository\CacheRepository;
use AlbertTrias\Performance\Domain\Repository\DatabaseRepository;
use AlbertTrias\Performance\Domain\Repository\SearchRepository;
use AlbertTrias\Performance\Infrastructure\Repository\ElasticSearchRepository;
use AlbertTrias\Performance\Infrastructure\Repository\MysqlDatabaseRepository;
use AlbertTrias\Performance\Infrastructure\Repository\RedisCacheRepository;
use PHPUnit\Framework\TestCase;

final class UpdateImageUseCaseTest extends TestCase
{
    /** @var DatabaseRepository $dbRepository */
    private $dbRepository;
    /** @var CacheRepository $cacheRepository */
    private $cacheRepository;
    /** @var SearchRepository $searchRepository */
    private $searchRepository;
    /** @var UpdateImageUseCase $updateImageUseCase */
    private $updateImageUseCase;
    /** @var Image $image */
    private $image;

    protected function setUp()
    {
        parent::setUp();

        $this->dbRepository = new MysqlDatabaseRepository('image_database_test');
        $this->cacheRepository = new RedisCacheRepository();
        $this->searchRepository = new ElasticSearchRepository();

        $this->updateImageUseCase = new UpdateImageUseCase(
            $this->dbRepository,
            $this->cacheRepository,
            $this->searchRepository
        );

        $this->image = new Image();
        $this->image->setName("name");
        $this->image->setUrl("http://name.com");
        $this->image->setTags("name");
        $this->image->setDescription("name");

        $this->dbRepository->save($this->image);
        $this->cacheRepository->store($this->image);
        $this->searchRepository->addElementToSearch($this->image);
    }

    /** @test */
    public function it_should_update_values_inside_db_and_cache_properly()
    {
        $this->image->setTags('name, name2');
        $this->updateImageUseCase->__invoke($this->image);

        $this->assertEquals(
            $this->dbRepository->fetch($this->image->getId()),
            $this->cacheRepository->fetch($this->image->getId())
        );
    }

    /** @test */
    public function it_should_update_values_inside_db_and_elastic_properly()
    {
        $this->image->setTags('name, name3');
        $this->updateImageUseCase->__invoke($this->image);

        $this->assertEquals(
            $this->dbRepository->fetch($this->image->getId()),
            $this->searchRepository->find($this->image->getId())
        );
    }

    protected function tearDown()
    {
        parent::tearDown();
    }
}