<?php

declare(strict_types=1);

namespace AlbertTrias\Performance\Test\Application;

use AlbertTrias\Performance\Application\ObtainAllImagesUseCase;
use AlbertTrias\Performance\Domain\Entity\Image;
use AlbertTrias\Performance\Domain\Repository\CacheRepository;
use AlbertTrias\Performance\Domain\Repository\DatabaseRepository;
use AlbertTrias\Performance\Infrastructure\Repository\MysqlDatabaseRepository;
use AlbertTrias\Performance\Infrastructure\Repository\RedisCacheRepository;
use PHPUnit\Framework\TestCase;

final class ObtainAllImagesUseCaseTest extends TestCase
{
    /** @var DatabaseRepository $dbRepository */
    private $dbRepository;
    /** @var CacheRepository $cacheRepository */
    private $cacheRepository;
    /** @var ObtainAllImagesUseCase $updateImageUseCase */
    private $obtainAllImagesUseCase;
    /** @var Image[] $image */
    private $images;



    protected function setUp()
    {
        parent::setUp();

        $this->dbRepository = new MysqlDatabaseRepository('image_database_test');
        $this->cacheRepository = new RedisCacheRepository();

        $this->obtainAllImagesUseCase = new ObtainAllImagesUseCase(
            $this->dbRepository,
            $this->cacheRepository
        );

        $image = new Image();
        $image->setName("omg");
        $image->setUrl("http://omg.com");
        $image->setTags("omg");
        $image->setDescription("omg");

        $this->dbRepository->save($image);
        $this->cacheRepository->store($image);

        $this->images = array($image);

        $image = new Image();
        $image->setName("wtf");
        $image->setUrl("http://wtf.com");
        $image->setTags("wtf");
        $image->setDescription("wtf");

        $this->dbRepository->save($image);
        $this->cacheRepository->store($image);

        $this->images[] = $image;
    }

    /** @test */
    public function it_should_return_all_images_stored()
    {
        $results = $this->obtainAllImagesUseCase->__invoke();

        $realSearch = array_filter($results, function(Image $imageResult) {
            return $imageResult->getName() === "omg" || $imageResult->getName() === "wtf";
        });

        usort($realSearch, function(Image $imageA, Image $imageB) {
            return ($imageA->getId() < $imageB->getId()) ? -1 : 1;
        });

        $this->assertEquals(
            $this->images,
            $realSearch
        );
    }

}