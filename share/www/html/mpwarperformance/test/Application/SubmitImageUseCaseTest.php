<?php

declare(strict_types=1);

namespace AlbertTrias\Performance\Test\Application;

use AlbertTrias\Performance\Application\ObtainAllImagesUseCase;
use AlbertTrias\Performance\Application\SubmitImageUseCase;
use AlbertTrias\Performance\Domain\Entity\Image;
use AlbertTrias\Performance\Domain\Repository\CacheRepository;
use AlbertTrias\Performance\Domain\Repository\DatabaseRepository;
use AlbertTrias\Performance\Infrastructure\Repository\ElasticSearchRepository;
use AlbertTrias\Performance\Infrastructure\Repository\MysqlDatabaseRepository;
use AlbertTrias\Performance\Infrastructure\Repository\RedisCacheRepository;
use PHPUnit\Framework\TestCase;

final class SubmitImageUseCaseTest extends TestCase
{
    /** @var DatabaseRepository $dbRepository */
    private $dbRepository;
    /** @var CacheRepository $cacheRepository */
    private $cacheRepository;
    /** @var SubmitImageUseCase $submitImageUseCase */
    private $submitImageUseCase;
    /** @var ElasticSearchRepository */
    private $searchRepository;
    /** @var Image $image */
    private $image;

    protected function setUp()
    {
        parent::setUp();

        $this->dbRepository = new MysqlDatabaseRepository('image_database_test');
        $this->cacheRepository = new RedisCacheRepository();
        $this->searchRepository = new ElasticSearchRepository();

        $this->submitImageUseCase = new SubmitImageUseCase(
            $this->dbRepository,
            $this->cacheRepository,
            $this->searchRepository
        );

        $image = new Image();
        $image->setName("lol");
        $image->setUrl("http://lol.com");
        $image->setTags("lol");
        $image->setDescription("lol");

        $this->dbRepository->save($image);
        $this->cacheRepository->store($image);

        $this->image = $image;
    }

    /** @test */
    public function it_should_store_the_image_properly()
    {
        $this->submitImageUseCase->__invoke($this->image);

        $this->assertEquals(
            $this->image,
            $this->dbRepository->fetch($this->image->getId())
        );
    }

}