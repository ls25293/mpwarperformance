<?php

require_once '../init.php';

use AlbertTrias\Performance\Controller\ImageController;

$actual_link = explode('/mpwarperformance',"$_SERVER[REQUEST_URI]");

$imageController = new ImageController();

if ($actual_link[1] === '/submit') {
    $imageController->submit();
} else if ($actual_link[1] === '/index') {
    $imageController->index();
} else if ($actual_link[1] === '/search') {
    $imageController->search();
} else {
    http_response_code(404);
    include(__RESOURCES__.'/errors/404.php');
}

	