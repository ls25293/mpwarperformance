<?php

use Doctrine\ORM\EntityManager;
use Doctrine\ORM\ORMException;
use Doctrine\ORM\Tools\Setup;

require_once "init.php";

$nameDatabase = "image_database";

$conn = array(
    'driver' => 'pdo_mysql',
    'host' => '127.0.0.1',
    'dbname' => $nameDatabase,
    'user' => 'root',
    'password' => 'root'
);

$config = Setup::createYAMLMetadataConfiguration(
    array(__CONFIG__ . "/doctrine"),
    true
);

$config->addEntityNamespace('', 'AlbertTrias\Performance\Domain\Entity');

try {
    $entityManager = EntityManager::create($conn, $config);
} catch (ORMException $e) {
    print($e->getMessage()."\n");
}

return \Doctrine\ORM\Tools\Console\ConsoleRunner::createHelperSet($entityManager);