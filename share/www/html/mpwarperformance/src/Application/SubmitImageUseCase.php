<?php

declare(strict_types=1);

namespace AlbertTrias\Performance\Application;

use AlbertTrias\Performance\Domain\Entity\Image;
use AlbertTrias\Performance\Domain\Repository\DatabaseRepository;
use AlbertTrias\Performance\Domain\Repository\SearchRepository;
use AlbertTrias\Performance\Infrastructure\Repository\RedisCacheRepository;

final class SubmitImageUseCase
{
    private $dbRepository;
    private $cacheRepository;
    private $searchRepository;

    public function __construct(DatabaseRepository $dbRepository,
                                RedisCacheRepository $cacheRepository,
                                SearchRepository $searchRepository)
    {
        $this->dbRepository = $dbRepository;
        $this->cacheRepository = $cacheRepository;
        $this->searchRepository = $searchRepository;
    }

    public function __invoke(Image $image): void
    {
        $this->dbRepository->save($image);

        $this->cacheRepository->store($image);

        $this->searchRepository->addElementToSearch($image);
    }
}