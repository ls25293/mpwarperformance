<?php

declare(strict_types=1);

namespace AlbertTrias\Performance\Application;

use AlbertTrias\Performance\Domain\Entity\Image;
use AlbertTrias\Performance\Domain\Repository\CacheRepository;
use AlbertTrias\Performance\Domain\Repository\DatabaseRepository;
use AlbertTrias\Performance\Domain\Repository\SearchRepository;

final class UpdateImageUseCase
{
    private $dbRepository;
    private $cacheRepository;
    private $searchRepository;

    public function __construct(DatabaseRepository $dbRepository,
                                CacheRepository $cacheRepository,
                                SearchRepository $searchRepository)
    {
        $this->dbRepository = $dbRepository;
        $this->cacheRepository = $cacheRepository;
        $this->searchRepository = $searchRepository;
    }

    public function __invoke(Image $image): bool
    {
        $imageUpdate = $this->dbRepository->update($image);
        return $this->cacheRepository->store($imageUpdate)
            && $this->searchRepository->updateElementFromSearch($imageUpdate);
    }
}