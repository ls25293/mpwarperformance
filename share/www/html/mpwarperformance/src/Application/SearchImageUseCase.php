<?php

declare(strict_types=1);

namespace AlbertTrias\Performance\Application;

use AlbertTrias\Performance\Domain\Repository\CacheRepository;
use AlbertTrias\Performance\Domain\Repository\DatabaseRepository;
use AlbertTrias\Performance\Domain\Repository\SearchRepository;

final class SearchImageUseCase
{
    private $searchRepository;
    private $dbRepository;
    private $cacheRepository;

    public function __construct(DatabaseRepository $dbRepository,
                                CacheRepository $cacheRepository,
                                SearchRepository $searchRepository)
    {
        $this->searchRepository = $searchRepository;
        $this->dbRepository = $dbRepository;
        $this->cacheRepository = $cacheRepository;
    }

    public function __invoke(string $query, string $value): array
    {
        $images = $this->searchRepository->searchBy($query, $value);
        if (empty($images)) {
            $images = $this->cacheRepository->searchBy($query, $value);

            if (!empty($images)) {
                foreach ($images as $image) {
                    $this->searchRepository->addElementToSearch($image);
                }

            }
        } else {
            return $images;
        }

        if (empty($images)) {
            $images = $this->dbRepository->searchBy($query, $value);

            if (!empty($images)) {
                foreach ($images as $image) {
                    $this->cacheRepository->store($image);
                    $this->searchRepository->addElementToSearch($image);
                }
            }

        }

        return $images;
    }
}