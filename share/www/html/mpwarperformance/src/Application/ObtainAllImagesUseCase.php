<?php

declare(strict_types=1);

namespace AlbertTrias\Performance\Application;

use AlbertTrias\Performance\Domain\Repository\CacheRepository;
use AlbertTrias\Performance\Domain\Repository\DatabaseRepository;

final class ObtainAllImagesUseCase
{
    private $dbRepository;
    private $cacheRepository;

    public function __construct(DatabaseRepository $dbRepository, CacheRepository $cacheRepository)
    {
        $this->dbRepository = $dbRepository;
        $this->cacheRepository = $cacheRepository;
    }

    public function __invoke(): array
    {
        $images = $this->cacheRepository->fetchAll();

        if (empty($images)) {
            $images = $this->dbRepository->fetchAll();
            foreach($images as $image) {
                $this->cacheRepository->store($image);
            }
        }
        return $images;
    }
}