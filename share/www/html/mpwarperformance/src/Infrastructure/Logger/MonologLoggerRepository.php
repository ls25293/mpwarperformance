<?php

declare(strict_types=1);

namespace AlbertTrias\Performance\Infrastructure\Logger;

use AlbertTrias\Performance\Domain\Logger\LoggerRepository;
use Monolog\Formatter\JsonFormatter;
use Monolog\Handler\RotatingFileHandler;
use Monolog\Logger;

final class MonologLoggerRepository implements LoggerRepository
{
    public const ERROR = 'ERROR';

    private $logger;

    public function __construct(string $name)
    {
        $this->logger = new Logger($name);
        $handler = new RotatingFileHandler(
            __LOGS__.'/'.$name,
            0,
            Logger::ERROR
        );
        $jsonFormatter = new JsonFormatter();
        $handler->setFormatter($jsonFormatter);
        $this->logger->pushHandler($handler);
    }

    public function log(string $level, string $message): void
    {
        $this->logger->log($level, $message);
    }

}