<?php

declare(strict_types=1);


namespace AlbertTrias\Performance\Infrastructure\Repository;

use AlbertTrias\Performance\Domain\Entity\Image;
use AlbertTrias\Performance\Domain\Repository\CacheRepository;
use AlbertTrias\Performance\Domain\Validator\SearchQueryFieldValidator;
use Predis\Client as PredisClient;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;


final class RedisCacheRepository implements CacheRepository
{
    private const INDEX = 'image';

    private $client;
    private $serializer;
    private $searchQueryFieldValidator;

    public function __construct()
    {
        $this->client = new PredisClient();

        $encoders = array(new JsonEncoder());
        $normalizers = array(new ObjectNormalizer());
        $this->serializer = new Serializer($normalizers, $encoders);

        $this->searchQueryFieldValidator = new SearchQueryFieldValidator();
    }

    public function store(Image $image): bool
    {
        $this->client->set(
            self::INDEX . "-" . $image->getId(),
            $this->serializer->serialize($image, 'json')
        );

        return true;
    }

    public function fetchAll(): array
    {
        $keys = $this->client->keys(self::INDEX . '-*');
        if (empty($keys)) return array();

        return array_map(function ($key) {
            return $this->serializer->deserialize(
                $this->client->get($key),
                Image::class,
                'json'
            );
        }, $keys);
    }

    public function fetch(int $id): ?Image
    {
        return $this->serializer->deserialize(
            $this->client->get(self::INDEX . '-' . $id),
            Image::class,
            'json'
        );
    }

    public function searchBy(string $query, string $value): array
    {
        if (!$this->searchQueryFieldValidator->validate($query)) {
            return array();
        }

        $images = $this->fetchAll();
        if (!empty($images)) {
            $images = array_filter($images, function(Image $image) use ($query, $value) {
                switch($query) {
                    case "name":
                        return strpos($image->getName(),$value) !== false;
                    case "tags":
                        return !is_null($image->getTags())
                            && strpos($image->getTags(),$value) !== false;
                    case "description":
                        return !is_null($image->getDescription())
                            && strpos($image->getDescription(),$value) !== false;
                }

                return false;
            });
        }

        return $images;
    }

    public function delete(int $id): bool
    {
        if (!$this->imageExists($id)) {
            return false;
        }
        $this->client->del(array(self::INDEX . '-' . $id));
        return true;
    }

    private function imageExists(int $id)
    {
        return $this->client->exists(self::INDEX . '-' . $id);
    }
}