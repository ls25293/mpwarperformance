<?php

declare(strict_types=1);

namespace AlbertTrias\Performance\Infrastructure\Repository;

use AlbertTrias\Performance\Domain\Entity\Image;
use AlbertTrias\Performance\Domain\Repository\DatabaseRepository;
use AlbertTrias\Performance\Domain\Validator\SearchQueryFieldValidator;
use AlbertTrias\Performance\Infrastructure\Logger\MonologLoggerRepository;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use Doctrine\ORM\Tools\Setup;
use Doctrine\ORM\TransactionRequiredException;

final class MysqlDatabaseRepository implements DatabaseRepository
{
    private const DOCTRINE_LOG_ERROR_FILE = 'doctrine_error';

    private $entityManager;
    private $logger;
    private $searchQueryFieldValidator;

    public function __construct(string $database)
    {
        $this->logger = new MonologLoggerRepository(self::DOCTRINE_LOG_ERROR_FILE);
        $this->searchQueryFieldValidator = new SearchQueryFieldValidator();

        $conn = array(
            'driver' => 'pdo_mysql',
            'host' => '127.0.0.1',
            'dbname' => $database,
            'user' => 'root',
            'password' => 'root'
        );

        $config = Setup::createYAMLMetadataConfiguration(
            array(__CONFIG__ . "/doctrine"),
            true
        );

        $config->addEntityNamespace('', 'AlbertTrias\Performance\Domain\Entity');

        try {
            $this->entityManager = EntityManager::create($conn, $config);
        } catch (ORMException $e) {
            $this->logger->log($this->logger::ERROR, $e->getMessage());
        }
    }

    public function save(Image $image): bool
    {
        try {
            $this->entityManager->persist($image);
            $this->entityManager->flush();
        } catch (ORMException $e) {
            $this->logger->log($this->logger::ERROR, $e->getMessage());
            return false;
        }

        return true;
    }

    public function fetchAll(): array
    {
        $queryBuilder = $this->entityManager->createQueryBuilder();
        $query = $queryBuilder
            ->select('i')
            ->from(Image::class, 'i')
            ->orderBy('i.id', 'asc')
            ->getQuery();

        return $query->getResult();
    }

    public function fetch(int $id): ?Image
    {
        try {
            /** @var Image $image */
            $image = $this->entityManager->find(Image::class, $id);
            if ($this->isImageFound($image)) {
                return $image;
            }

        } catch (OptimisticLockException $e) {
            $this->logger->log($this->logger::ERROR, $e->getMessage());
        } catch (TransactionRequiredException $e) {
            $this->logger->log($this->logger::ERROR, $e->getMessage());
        } catch (ORMException $e) {
            $this->logger->log($this->logger::ERROR, $e->getMessage());
        }

        return null;
    }

    public function delete(int $id): bool
    {
        try {
            /** @var Image $image */
            $image = $this->entityManager->find(Image::class, $id);
            if (!$this->isImageFound($image)) {
                return false;
            }

            $this->entityManager->remove($image);
            $this->entityManager->flush();

            return true;

        } catch (OptimisticLockException $e) {
            $this->logger->log($this->logger::ERROR, $e->getMessage());
        } catch (TransactionRequiredException $e) {
            $this->logger->log($this->logger::ERROR, $e->getMessage());;
        } catch (ORMException $e) {
            $this->logger->log($this->logger::ERROR, $e->getMessage());
        }

        return false;
    }

    public function update(Image $image): ?Image
    {
        try {
            /** @var Image $imageFind */
            $imageFind = $this->entityManager->find(Image::class, $image->getId());
            if (!$this->isImageFound($imageFind)) {
                return null;
            }

            $imageFind->setName($image->getName());
            $imageFind->setTags($image->getTags());
            $imageFind->setDescription($image->getDescription());

            $this->entityManager->flush();

            return $imageFind;

        } catch (OptimisticLockException $e) {
            $this->logger->log($this->logger::ERROR, $e->getMessage());
        } catch (TransactionRequiredException $e) {
            $this->logger->log($this->logger::ERROR, $e->getMessage());
        } catch (ORMException $e) {
            $this->logger->log($this->logger::ERROR, $e->getMessage());
        }

        return null;
    }

    public function searchBy(string $query, string $value): array
    {
        if (!$this->searchQueryFieldValidator->validate($query)) {
            return array();
        }

        $queryBuilder = $this->entityManager->createQueryBuilder();
        $query = $queryBuilder
            ->select('i')
            ->from(Image::class, 'i')
            ->where($queryBuilder->expr()->like('i.' . $query, ':value'))
            ->orderBy('i.id', 'asc')
            ->setParameter('value','%'.$value.'%')
            ->getQuery();

        return $query->getResult();
    }

    private function isImageFound(?Image $image)
    {
        return (is_null($image)) ? false : true;
    }
}