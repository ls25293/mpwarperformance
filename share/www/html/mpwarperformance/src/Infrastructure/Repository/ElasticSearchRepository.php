<?php

declare(strict_types=1);

namespace AlbertTrias\Performance\Infrastructure\Repository;

use AlbertTrias\Performance\Domain\Entity\Image;
use AlbertTrias\Performance\Domain\Repository\SearchRepository;
use AlbertTrias\Performance\Domain\Validator\SearchQueryFieldValidator;
use Elasticsearch\ClientBuilder;

final class ElasticSearchRepository implements SearchRepository
{
    private const INDEX = 'performance';
    private const TYPE = 'image';

    private $client;
    private $searchQueryFieldValidator;

    public function __construct()
    {
        $this->client = ClientBuilder::create()->build();

        $this->searchQueryFieldValidator = new SearchQueryFieldValidator();
    }

    public function searchBy(string $query, string $value): array
    {
        if (!$this->searchQueryFieldValidator->validate($query)) {
            return array();
        }

        if (!$this->indexExists(self::INDEX)) {
            return array();
        }

        $params = [
            'index' => self::INDEX,
            'type' => self::TYPE,
            'body' => [
                'query' => [
                    'wildcard' => [
                        $query => '*' . $value . '*'
                    ],
                ]
            ]
        ];

        $response = $this->client->search($params);

        if ($response['hits']['total'] === 0) {
            return array();
        }

        return array_map(function ($hit) {
            $image = new Image();
            $image->setId($hit['_source']['id']);
            $image->setName($hit['_source']['name']);
            $image->setUrl($hit['_source']['url']);
            $image->setTags($hit['_source']['tags']);
            $image->setDescription($hit['_source']['description']);

            return $image;
        }, $response['hits']['hits']);

    }

    public function addElementToSearch(Image $image): bool
    {
        $params = [
            'index' => self::INDEX,
            'type' => self::TYPE,
            'id' => $image->getId(),
            'body' => [
                'id' => $image->getId(),
                'url' => $image->getUrl(),
                'name' => $image->getName(),
                'tags' => $image->getTags(),
                'description' => $image->getDescription()
            ]
        ];


        $this->client->index($params);
        return true;
    }

    public function updateElementFromSearch(Image $image): bool
    {
        if (!$this->documentExists(self::INDEX, self::TYPE, $image->getId())) {
            $this->addElementToSearch($image);
        } else {
            $params = [
                'index' => self::INDEX,
                'type' => self::TYPE,
                'id' => $image->getId(),
                'body' => [
                    'doc' => [
                        'url' => $image->getUrl(),
                        'name' => $image->getName(),
                        'tags' => $image->getTags(),
                        'description' => $image->getDescription()
                    ]
                ]
            ];
            $this->client->update($params);
        }
        return true;
    }

    public function delete(int $id): bool
    {
        if (!$this->documentExists(self::INDEX, self::TYPE, $id)) {
            return false;
        }

        $params = [
            'index' => self::INDEX,
            'type' => self::TYPE,
            'id' => $id
        ];

        $this->client->delete($params);
        return true;
    }

    public function find(int $id): ?Image
    {
        $params = [
            'index' => self::INDEX,
            'type' => self::TYPE,
            'id' => $id
        ];

        $response = $this->client->get($params);

        if (!$response['found']) {
            return null;
        }

        $hit = $response['_source'];
        $image = new Image();
        $image->setId($hit['id']);
        $image->setName($hit['name']);
        $image->setUrl($hit['url']);
        $image->setTags($hit['tags']);
        $image->setDescription($hit['description']);

        return $image;
    }

    private function indexExists(string $index): bool
    {
        $indexParam['index'] = $index;
        return $this->client->indices()->exists($indexParam);
    }

    private function documentExists(string $index, string $type, int $documentId): bool
    {
        $params = [
            'index' => $index,
            'type' => $type,
            'id' => $documentId
        ];
        return $this->client->exists($params);
    }
}