<?php

declare(strict_types=1);


namespace AlbertTrias\Performance\Domain\Repository;


use AlbertTrias\Performance\Domain\Entity\Image;

interface SearchRepository
{
    public function searchBy(string $query, string $value): array;

    public function updateElementFromSearch(Image $image): bool;

    public function addElementToSearch(Image $image): bool;

    public function delete(int $id): bool;

    public function find(int $id): ?Image;
}