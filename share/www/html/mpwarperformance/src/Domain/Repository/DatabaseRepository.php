<?php

declare(strict_types=1);

namespace AlbertTrias\Performance\Domain\Repository;

use AlbertTrias\Performance\Domain\Entity\Image;

interface DatabaseRepository
{
    public function save(Image $image): bool;

    public function fetchAll(): array;

    public function fetch(int $id): ?Image;

    public function delete(int $id): bool;

    public function update(Image $image): ?Image;

    public function searchBy(string $query, string $value): array;
}