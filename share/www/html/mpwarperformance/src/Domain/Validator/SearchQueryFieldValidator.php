<?php

declare(strict_types=1);

namespace AlbertTrias\Performance\Domain\Validator;

final class SearchQueryFieldValidator
{
    private $queryFields;

    public function __construct()
    {
       $this->queryFields = array(
           "name",
           "tags",
           "description"
       );
    }

    public function validate(string $queryField): bool
    {
        return in_array($queryField, $this->queryFields);
    }
}