<?php

declare(strict_types=1);

namespace AlbertTrias\Performance\Domain\Logger;

interface LoggerRepository
{
    public function log(string $level, string $message): void;
}