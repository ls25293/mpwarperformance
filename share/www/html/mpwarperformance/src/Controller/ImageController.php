<?php

declare(strict_types=1);

namespace AlbertTrias\Performance\Controller;

use AlbertTrias\Performance\Application\ObtainAllImagesUseCase;
use AlbertTrias\Performance\Application\SearchImageUseCase;
use AlbertTrias\Performance\Application\UpdateImageUseCase;
use AlbertTrias\Performance\Domain\Entity\Image;
use AlbertTrias\Performance\Infrastructure\Logger\MonologLoggerRepository;
use AlbertTrias\Performance\Infrastructure\Repository\ElasticSearchRepository;
use AlbertTrias\Performance\Infrastructure\Repository\MysqlDatabaseRepository;
use AlbertTrias\Performance\Infrastructure\Repository\RedisCacheRepository;
use Monolog\Formatter\JsonFormatter;
use Monolog\Handler\RotatingFileHandler;
use Monolog\Handler\StreamHandler;
use Monolog\Logger;
use Twig_Environment;
use Twig_Loader_Filesystem;

final class ImageController
{
    private const TWIG_LOG_ERROR_FILE = 'twig_error';
    private const TEMPLATES_FOLDER = '/templates';
    private const DATABASE = 'image_database';

    private $twig;
    private $obtainAllImagesUseCase;
    private $updateImageUseCase;
    private $searchImageUseCase;
    private $logger;

    public function __construct()
    {
        $loader = new Twig_Loader_Filesystem(__RESOURCES__ . self::TEMPLATES_FOLDER);
        $this->twig = new Twig_Environment($loader, array(
            'auto_reload' => true,
            'cache' => __CACHE__
        ));

        $this->logger = new MonologLoggerRepository(self::TWIG_LOG_ERROR_FILE);

        $dbRepository = new MysqlDatabaseRepository(self::DATABASE);
        $cacheRepository = new RedisCacheRepository();
        $searchRepository = new ElasticSearchRepository();

        $this->obtainAllImagesUseCase = new ObtainAllImagesUseCase(
            $dbRepository,
            $cacheRepository
        );

        $this->updateImageUseCase = new UpdateImageUseCase(
            $dbRepository,
            $cacheRepository,
            $searchRepository
        );

        $this->searchImageUseCase = new SearchImageUseCase(
            $dbRepository,
            $cacheRepository,
            $searchRepository
        );
    }

    public function index()
    {
        if ($_SERVER['REQUEST_METHOD'] === 'POST') {
            $image = new Image();
            $id = $_POST['id'];

            $image->setId($id);
            $image->setName($_POST["image-$id-name"]);
            $image->setTags($_POST["image-$id-tags"]);
            $image->setDescription($_POST["image-$id-description"]);

            $this->updateImageUseCase->__invoke($image);
        }

        try {
            echo $this->twig->render('index.html.twig', array(
                'title' => 'Search Image',
                'description' => 'Search your images here using tags',
                'images' => $this->obtainAllImagesUseCase->__invoke()
            ));
        } catch (\Twig_Error_Loader $e) {
            $this->logger->log($this->logger::ERROR, $e->getMessage());
            http_response_code(500);
            include(__RESOURCES__.'/errors/500.php');
        } catch (\Twig_Error_Runtime $e) {
            $this->logger->log($this->logger::ERROR, $e->getMessage());
            http_response_code(500);
            include(__RESOURCES__.'/errors/500.php');
        } catch (\Twig_Error_Syntax $e) {
            $this->logger->log($this->logger::ERROR, $e->getMessage());
            http_response_code(500);
            include(__RESOURCES__.'/errors/500.php');
        }
    }

    public function submit()
    {
        try {
            echo $this->twig->render('submit.html.twig', array(
                'title' => 'Submit Image',
                'description' => 'Submit your images here'
            ));
        } catch (\Twig_Error_Loader $e) {
            $this->logger->log($this->logger::ERROR, $e->getMessage());
            http_response_code(500);
            include(__RESOURCES__.'/errors/500.php');
        } catch (\Twig_Error_Runtime $e) {
            $this->logger->log($this->logger::ERROR, $e->getMessage());
            http_response_code(500);
            include(__RESOURCES__.'/errors/500.php');
        } catch (\Twig_Error_Syntax $e) {
            $this->logger->log($this->logger::ERROR, $e->getMessage());
            http_response_code(500);
            include(__RESOURCES__.'/errors/500.php');
        }
    }

    public function search()
    {
        $images = null;
        if ($_SERVER['REQUEST_METHOD'] === 'POST') {
            $query = $_POST['query'];
            $value = $_POST["search-$query"];

            $images = $this->searchImageUseCase->__invoke($query, $value);
        }

        try {
            echo $this->twig->render('search.html.twig', array(
                'title' => 'Search Image',
                'description' => 'Search your images here',
                'images' => $images
            ));
        } catch (\Twig_Error_Loader $e) {
            $this->logger->log($this->logger::ERROR, $e->getMessage());
            http_response_code(500);
            include(__RESOURCES__.'/errors/500.php');
        } catch (\Twig_Error_Runtime $e) {
            $this->logger->log($this->logger::ERROR, $e->getMessage());
            http_response_code(500);
            include(__RESOURCES__.'/errors/500.php');
        } catch (\Twig_Error_Syntax $e) {
            $this->logger->log($this->logger::ERROR, $e->getMessage());
            http_response_code(500);
            include(__RESOURCES__.'/errors/500.php');
        }
    }
}