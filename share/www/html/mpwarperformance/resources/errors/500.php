<!DOCTYPE html>
<html lang="en">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <meta name="description" content="There's been an internal error in the server"/>

        <title>500 - Internal Error</title>
    </head>
    <body>
        <h1>500 - INTERNAL ERROR</h1>
        <p>There's been an internal error in the server. Contact Administrator.</p>
    </body>
</html>
