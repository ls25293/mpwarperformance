<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <meta name="description" content="The page you've selected doesn't exist"/>

    <title>404 - Page Not Found</title>
</head>
<body>
<h1>404 - PAGE NOT FOUND</h1>
<p>The page you've selected doesn't exist.</p>
</body>
</html>
