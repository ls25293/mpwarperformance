<?php

require_once __DIR__ . '/../../init.php';

use Gumlet\ImageResize;
use PhpAmqpLib\Connection\AMQPStreamConnection;
use PhpAmqpLib\Message\AMQPMessage;

$connection = new AMQPStreamConnection('localhost', 5672, 'admin', 'admin');
$channel = $connection->channel();

$channel->exchange_declare('topic_logs', 'topic', false, false, false);

list($queue_name, ,) = $channel->queue_declare("images-resize-big-queue", false, false, false, false);

$binding_keys = [
    'images.resize.big.all',
    'images.resize.big.125',
    'images.resize.big.150',
    'images.resize.big.175'
];

foreach($binding_keys as $binding_key) {
    $channel->queue_bind($queue_name, 'topic_logs', $binding_key);
}

echo ' [*] Waiting for logs. To exit press CTRL+C', "\n";

$callback = function(AMQPMessage $msg){
    $image = new ImageResize(__IMAGES__ . '/' . $msg->body);
    $option = explode('.',$msg->delivery_info['routing_key']);
    $msgDatabase = "{}";

    switch($option[3]) {
        case 'all':
            $image
                ->scale(125)
                ->save(__IMAGES__ . '/125-' . $msg->body)
                ->scale(150)
                ->save(__IMAGES__ . '/150-' . $msg->body)
                ->scale(175)
                ->save(__IMAGES__ . '/175-' . $msg->body);

            $msgDatabase = new AMQPMessage(
                '[{
                "name": "125-' . $msg->body . '",
                "url": "/mpwarperformance/var/images/125-' . $msg->body . '",
                "tags": "resizebig125"
                },
                {
                "name": "150-' . $msg->body . '",
                "url": "/mpwarperformance/var/images/150-' . $msg->body . '",
                "tags": "resizebig150"
                },
                {
                "name": "175-' . $msg->body . '",
                "url": "/mpwarperformance/var/images/175-' . $msg->body . '",
                "tags": "resizebig175"
                }]');
            break;

        case '125':
            $image
                ->scale(125)
                ->save(__IMAGES__ . '/125-' . $msg->body);

            $msgDatabase = new AMQPMessage(
                '{
                "name": "125-' . $msg->body . '",
                "url": "/mpwarperformance/var/images/125-' . $msg->body . '",
                "tags": "resizebig125"
                }');
            break;

        case '150':
            $image
                ->scale(150)
                ->save(__IMAGES__ . '/150-' . $msg->body);

            $msgDatabase = new AMQPMessage(
                '{
                "name": "150-' . $msg->body . '",
                "url": "/mpwarperformance/var/images/150-' . $msg->body . '",
                "tags": "resizebig150"
                }');
            break;

        case '175':
            $image
                ->scale(175)
                ->save(__IMAGES__ . '/175-' . $msg->body);

            $msgDatabase = new AMQPMessage(
                '{
                "name": "175-' . $msg->body . '",
                "url": "/mpwarperformance/var/images/175-' . $msg->body . '",
                "tags": "resizebig175"
                }');
            break;

        default:
            echo 'Error';
    }

    $routing_key = 'database';
    $connection = new AMQPStreamConnection('localhost', 5672, 'admin', 'admin');
    $channel = $connection->channel();
    $channel->exchange_declare('database_logs', 'topic', false, false, false);
    $channel->basic_publish($msgDatabase, 'database_logs', $routing_key);
    $channel->close();
    $connection->close();
};

$channel->basic_consume($queue_name, '', false, true, false, false, $callback);

while(count($channel->callbacks)) {
    $channel->wait();
}

$channel->close();
$connection->close();