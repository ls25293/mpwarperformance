<?php

require_once __DIR__ . '/../../init.php';

use Gumlet\ImageResize;
use PhpAmqpLib\Connection\AMQPStreamConnection;
use PhpAmqpLib\Message\AMQPMessage;

$connection = new AMQPStreamConnection('localhost', 5672, 'admin', 'admin');
$channel = $connection->channel();

$channel->exchange_declare('topic_logs', 'topic', false, false, false);

list($queue_name, ,) = $channel->queue_declare("images-filter-queue", false, false, false, false);

$binding_keys = [
    'images.filter.all',
    'images.filter.blur',
    'images.filter.gray',
    'images.filter.pixel'
];

foreach($binding_keys as $binding_key) {
    $channel->queue_bind($queue_name, 'topic_logs', $binding_key);
}

echo ' [*] Waiting for logs. To exit press CTRL+C', "\n";

$callback = function(AMQPMessage $msg){
    $image = new ImageResize(__IMAGES__ . '/' . $msg->body);
    $option = explode('.',$msg->delivery_info['routing_key']);
    $msgDatabase = "{}";

    switch($option[2]) {
        case 'all':
            $image
                ->addFilter(function ($imageDesc) {
                    imagefilter($imageDesc, IMG_FILTER_GAUSSIAN_BLUR,100);
                })
                ->save(__IMAGES__ . '/blur-' . $msg->body)
                ->addFilter(function ($imageDesc) {
                    imagefilter($imageDesc, IMG_FILTER_GRAYSCALE,100);
                })
                ->save(__IMAGES__ . '/gray-' . $msg->body)
                ->addFilter(function ($imageDesc) {
                    imagefilter($imageDesc, IMG_FILTER_PIXELATE,100);
                })
                ->save(__IMAGES__ . '/pixel-' . $msg->body);

            $msgDatabase = new AMQPMessage(
                '[{
                "name": "blur-' . $msg->body . '",
                "url": "/mpwarperformance/var/images/blur-' . $msg->body . '",
                "tags": "filterblur"
                },
                {
                "name": "gray-' . $msg->body . '",
                "url": "/mpwarperformance/var/images/gray-' . $msg->body . '",
                "tags": "filtergray"
                },
                {
                "name": "pixel-' . $msg->body . '",
                "url": "/mpwarperformance/var/images/pixel-' . $msg->body . '",
                "tags": "filterpixel"
                }]');
            break;

        case 'blur':
            $image
                ->addFilter(function ($imageDesc) {
                    imagefilter($imageDesc, IMG_FILTER_GAUSSIAN_BLUR,100);
                })
                ->save(__IMAGES__ . '/blur-' . $msg->body);

            $msgDatabase = new AMQPMessage(
                '{
                "name": "blur-' . $msg->body . '",
                "url": "/mpwarperformance/var/images/blur-' . $msg->body . '",
                "tags": "filterblur"
                }');
            break;

        case 'gray':
            $image
                ->addFilter(function ($imageDesc) {
                    imagefilter($imageDesc, IMG_FILTER_GRAYSCALE,100);
                })
                ->save(__IMAGES__ . '/gray-' . $msg->body);

            $msgDatabase = new AMQPMessage(
                '{
                "name": "gray-' . $msg->body . '",
                "url": "/mpwarperformance/var/images/gray-' . $msg->body . '",
                "tags": "filtergray"
                }');
            break;

        case 'pixel':
            $image
                ->addFilter(function ($imageDesc) {
                    imagefilter($imageDesc, IMG_FILTER_PIXELATE,100);
                })
                ->save(__IMAGES__ . '/pixel-' . $msg->body);

            $msgDatabase = new AMQPMessage(
                '{
                "name": "pixel-' . $msg->body . '",
                "url": "/mpwarperformance/var/images/pixel-' . $msg->body . '",
                "tags": "filterpixel"
                }');
            break;

        default:
            echo 'Error';
    }

    $routing_key = 'database';
    $connection = new AMQPStreamConnection('localhost', 5672, 'admin', 'admin');
    $channel = $connection->channel();
    $channel->exchange_declare('database_logs', 'topic', false, false, false);
    $channel->basic_publish($msgDatabase, 'database_logs', $routing_key);
    $channel->close();
    $connection->close();
};

$channel->basic_consume($queue_name, '', false, true, false, false, $callback);

while(count($channel->callbacks)) {
    $channel->wait();
}

$channel->close();
$connection->close();


