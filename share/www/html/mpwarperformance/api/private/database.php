<?php

require_once __DIR__ . '/../../init.php';

use AlbertTrias\Performance\Application\SubmitImageUseCase;
use AlbertTrias\Performance\Domain\Entity\Image;
use AlbertTrias\Performance\Infrastructure\Repository\ElasticSearchRepository;
use AlbertTrias\Performance\Infrastructure\Repository\MysqlDatabaseRepository;
use AlbertTrias\Performance\Infrastructure\Repository\RedisCacheRepository;
use PhpAmqpLib\Connection\AMQPStreamConnection;
use PhpAmqpLib\Message\AMQPMessage;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\ArrayDenormalizer;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;

$connection = new AMQPStreamConnection('localhost', 5672, 'admin', 'admin');
$channel = $connection->channel();

$channel->exchange_declare('database_logs', 'topic', false, false, false);

list($queue_name, ,) = $channel->queue_declare("images-database", false, false, false, false);

$routing_key = 'database';
$channel->queue_bind($queue_name, 'database_logs', $routing_key);

echo ' [*] Waiting for logs. To exit press CTRL+C', "\n";

$callback = function(AMQPMessage $msg){
    $encoders = array(new JsonEncoder());

    $normalizers = array(new ObjectNormalizer(), new ArrayDenormalizer());

    $serializer = new Serializer($normalizers, $encoders);

    /** @var Image[] $images */
    $images = $serializer->deserialize($msg->body, Image::class.'[]', 'json');

    $submitImageUseCase = new SubmitImageUseCase(
        new MysqlDatabaseRepository('image_database'),
        new RedisCacheRepository(),
        new ElasticSearchRepository()
    );

    foreach($images as $image) $submitImageUseCase->__invoke($image);

};

$channel->basic_consume($queue_name, '', false, true, false, false, $callback);

while(count($channel->callbacks)) {
    $channel->wait();
}

$channel->close();
$connection->close();


