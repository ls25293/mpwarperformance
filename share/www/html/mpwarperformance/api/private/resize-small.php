<?php

require_once __DIR__ . '/../../init.php';

use Gumlet\ImageResize;
use PhpAmqpLib\Connection\AMQPStreamConnection;
use PhpAmqpLib\Message\AMQPMessage;

$connection = new AMQPStreamConnection('localhost', 5672, 'admin', 'admin');
$channel = $connection->channel();

$channel->exchange_declare('topic_logs', 'topic', false, false, false);

list($queue_name, ,) = $channel->queue_declare("images-resize-small-queue", false, false, false, false);

$binding_keys = [
    'images.resize.small.all',
    'images.resize.small.25',
    'images.resize.small.50',
    'images.resize.small.75'
];

foreach($binding_keys as $binding_key) {
    $channel->queue_bind($queue_name, 'topic_logs', $binding_key);
}

echo ' [*] Waiting for logs. To exit press CTRL+C', "\n";

$callback = function(AMQPMessage $msg){
    $image = new ImageResize(__IMAGES__ . '/' . $msg->body);
    $option = explode('.',$msg->delivery_info['routing_key']);
    $msgDatabase = "{}";

    switch($option[3]) {
        case 'all':
            $image
                ->scale(25)
                ->save(__IMAGES__ . '/25-' . $msg->body)
                ->scale(50)
                ->save(__IMAGES__ . '/50-' . $msg->body)
                ->scale(75)
                ->save(__IMAGES__ . '/75-' . $msg->body);

            $msgDatabase = new AMQPMessage(
                '[{
                "name": "25-' . $msg->body . '",
                "url": "/mpwarperformance/var/images/25-' . $msg->body . '",
                "tags": "resizesmall25"
                },
                {
                "name": "50-' . $msg->body . '",
                "url": "/mpwarperformance/var/images/50-' . $msg->body . '",
                "tags": "resizesmall50"
                },
                {
                "name": "75-' . $msg->body . '",
                "url": "/mpwarperformance/var/images/75-' . $msg->body . '",
                "tags": "resizesmall75"
                }]');
            break;
        case '25':
            $image
                ->scale(25)
                ->save(__IMAGES__ . '/25-' . $msg->body);
            $msgDatabase = new AMQPMessage(
                '{
                "name": "25-' . $msg->body . '",
                "url": "/mpwarperformance/var/images/25-' . $msg->body . '",
                "tags": "resizesmall25"
                }');
            break;
        case '50':
            $image
                ->scale(50)
                ->save(__IMAGES__ . '/50-' . $msg->body);
            $msgDatabase = new AMQPMessage(
                '{
                "name": "50-' . $msg->body . '",
                "url": "/mpwarperformance/var/images/50-' . $msg->body . '",
                "tags": "resizesmall50"
                }');
            break;
        case '75':
            $image
                ->scale(75)
                ->save(__IMAGES__ . '/75-' . $msg->body);
            $msgDatabase = new AMQPMessage(
                '{
                "name": "75-' . $msg->body . '",
                "url": "/mpwarperformance/var/images/75-' . $msg->body . '",
                "tags": "resizesmall75"
                }');
            break;
        default:
            echo 'Error';

    }

    $routing_key = 'database';
    $connection = new AMQPStreamConnection('localhost', 5672, 'admin', 'admin');
    $channel = $connection->channel();
    $channel->exchange_declare('database_logs', 'topic', false, false, false);
    $channel->basic_publish($msgDatabase, 'database_logs', $routing_key);
    $channel->close();
    $connection->close();
};

$channel->basic_consume($queue_name, '', false, true, false, false, $callback);

while(count($channel->callbacks)) {
    $channel->wait();
}

$channel->close();
$connection->close();


