<?php

require_once __DIR__ . '/../init.php';

use PhpAmqpLib\Connection\AMQPStreamConnection;
use PhpAmqpLib\Message\AMQPMessage;

if (!empty($_FILES)) {

    $tempFile = $_FILES['file']['tmp_name'];

    $targetFile =  __IMAGES__ . '/' . $_FILES['file']['name'];

    move_uploaded_file($tempFile,$targetFile);

    $connection = new AMQPStreamConnection('localhost', 5672, 'admin', 'admin');
    $channel = $connection->channel();

    $channel->exchange_declare('topic_logs', 'topic', false, false, false);

    $routing_keys = [
        'images.resize.big.all',
        'images.resize.small.all',
        'images.filter.all'
    ];

    $data = $_FILES['file']['name'];

    $msg = new AMQPMessage($data);
    foreach($routing_keys as $routing_key) {
        $channel->basic_publish($msg, 'topic_logs', $routing_key);
    }

    $channel->close();
    $connection->close();

}
