#Performance Project - Image Searcher

##Introduction
This project consists on the development of an image visualizer, uploader & searcher.
This image will have as info url, name, tags and descriptions. Except url, we will be able to change
the other fields and search by them. 

##Requirements
This image searcher must fulfill the following requirements:

1. Images must be uploaded to the web server with concurrency.
2. Images must be resized and filtered while preserving the originals, and this 
must be done with the use of queues, one for each type of transformation.
3. All images and their transformations must persist in a database.
4. To speed up the process of showing all images, we must use a Redis cache or similar.
5. To speed up the process of searching an image by name, tags or description,
we must use Elastic Search or similar.
6. We must publish a report which consists on an analysis of the website performance. We must use Blackfire
to show the results before and, if we do changes to improve it, after.

##Tools and Libraries Used
This set of libraries have been used to fulfill those requirements. Extra ones have also been used
as a personal choice to apply what I learnt in classes:

1. Dropzone to upload the images with concurrency.
2. PHP Image Resize to resize and filter images.
3. RabbitMQ to create and work with queues.
4. Doctrine to persist, update and fetch the information from a MySql database. 
Configuration is done with YAML, using the library from Symfony.
5. Redis Cache to speed up the process of fetching all images.
6. Elastic Search to speed up the process of searching images by fields.
7. Blackfire to analyse the website performance.
8. PHPUnit to test parts of the application.
9. Monolog to log any error that may happen.
10. Twig to render the web pages.

##Installation
To install this project properly:

1. Make sure to install Vagrant and Virtualbox beforehand, else you won't be able to install the virtual machine.
2. Download/Clone this repository to a location of your choice.
3. Head to the vagrant folder and do a ```vagrant up```. Configure the Vagrantfile according to your needs, though the settings put are the minimum recommended
due to Elastic Search and Kibana consuming a lot of resources. [1]
4. Once the machine is on, do a ```vagrant ssh``` and do a ```sudo yum install -y zip unzip```.
5. Head to the /var/www/html/mpwarperformance folder and do a ```composer install```. 
6. Execute the sql file inside the config/sql folder.
7. ```vendor/bin/doctrine orm:schema-tool:create``` in the same location as the cli-config
(if you want to try the test part, go to cli-config.php and change the name of the database to 
`image-database-test`)
to make doctrine create the necessary tables for this project to work.
8. Execute filter.php, resize-small.php, resize-big.php and database.php (in any order)

[1] If the vagrant up fails due to the "vsboxf" missing, turn off the machine and comment these lines from the Vagrantfile:
```
config.vm.synced_folder "../share/www", "/var/www",
	type: "virtualbox",
	owner: "vagrant",
	group: "apache",
	mount_options: ["dmode=777,fmode=777"]
```
Once you're done, turn on the machine. If the installation succeeds, turn it off, uncomment the lines above and turn it on again.

##Current websites available in the application:

1. http://<ip_server>/mpwarperformance/index: The index page.
2. http://<ip_server>/mpwarperformance/submit: The submit image page.
3. http://<ip_server>/mpwarperformance/search: The search image page.

##Plans for the future
1. Make it easier to change the user and pass used for the databases.
2. Instead of an SQL file, search for a different way to create databases 
without using MySql queries.
3. Change the logger so it uses the repository pattern, in case there's a need to change
it in the future.
4. Find how to handle paths so we don't need to create a mpwarperformance folder
for the project to work. 
5. Right now a route of `http://<ip>/mpwarperformance/` doesn't work with the project.
I need to figure out how to configure the .htaccess so this is possible.